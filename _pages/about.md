---
layout: page
title: About
permalink: /about/
sponsors: true
---

## Important Information

Linux.conf.au 2021 will be held online from January 23-25 2021.
The conference will be run in the Australian Eastern Daylight Time (AEDT - UTC+11) timezone.

## Linux.conf.au Background
Starting as CALU (Conference of Australian Linux Users) in 1999, the conference will be entering its 22<sup>nd</sup> year in 2021.

Linux.conf.au is a conference with a focus on Linux and the community that has built up around it and the values that it represents.
It is a deeply technical conference covering topics varying from the inner workings of the Linux kernel to the inner workings of dealing with communities.

Each year the conference attracts between 500-800 attendees making it one of the most popular grassroots open source conferences in our region.

Run by volunteers, linux.conf.au is a not for profit conference that aims to provide attendees with a world-class conference at a down to earth rate.

## Linux Australia
[Linux Australia](https://linux.org.au) represents approximately 5000 Australian users and developers of Free Software and Open Technologies, and facilitates internationally-renowned events including linux.conf.au -- Australasia's grassroots Free and Open Source Software Conference.

## Our Team
Linux.conf.au is organised by a core team of volunteers who contribute many hours of their time to run this event.
In addition to this core team there are many other contributors who put in significant effort to make this a successful event.

 * Sae Ra Germaine
 * Joel Addison
 * David Bell
