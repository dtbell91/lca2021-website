---
layout: page
title: Sponsors
permalink: /sponsors/
sponsors: false
---

We would like to thank all of our sponsors.
Without them the conference would not be possible.

If you are interested in supporting linux.conf.au 2021, we would [love to hear from you](/sponsors/prospectus/)!

{% include sponsors.html %}
