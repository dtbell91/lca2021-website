---
layout: page
title: Colophon
permalink: /colophon/
sponsors: true
---

### Website Copy

We stand on the shoulders of giants, both previous organisers of linux.conf.au and other events, including but not limited to:
 * [PyCon AU](https://2019.pycon-au.org/)

### Website Images

 * ["penguin group small"](https://www.flickr.com/photos/56521678@N04/5220534077) by [Antarctica Bound](https://www.flickr.com/photos/56521678@N04) is licensed under [CC BY-ND 2.0](https://creativecommons.org/licenses/by-nd/2.0/)
 * [Cory Doctorow](/news/keynote-cory-doctorow/) photo by Anna Olthoff

### Website Design

Brand for linux.conf.au by [Tania Walker](https://www.taniawalker.com)

This website is developed using free and open source software.

 * [Jekyll](https://jekyllrb.com/)
 * [Bootstrap](https://getbootstrap.com/)
 * [Project Zeppelin](https://github.com/gdg-x/zeppelin) (Jekyll configuration inspiration)
