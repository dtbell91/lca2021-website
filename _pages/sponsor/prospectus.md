---
layout: page
title: Sponsorship Prospectus
permalink: /sponsors/prospectus/
sponsors: false
---

## How to sponsor

Contact [sponsorship@lca2021.linux.org.au](mailto:sponsorship@lca2021.linux.org.au) for more information and a copy of our prospectus.

## Why Sponsor

### Overview

Sponsors of linux.conf.au have a unique opportunity to globally promote their brand to a large number of influential industry professionals.
One of the world's best free and open source technology conferences, linux.conf.au is regularly attended by over 600 delegates, more than half of whom attend as Professional Delegates.
Delegates travel from all over the world for a week of thought provoking keynotes from our internationally recognised invited speakers, high quality presentations and tutorials from the makers of these technologies, and connecting with other delegates in both organised networking sessions and spontaneous encounters in the hallways.

### Sponsor Early

Agreeing to sponsor linux.conf.au 2021 early is important.
The earlier you sign a sponsorship agreement, and we have a purchase order completed, the more time we can spend promoting your sponsorship.

### Diversity and Inclusion

linux.conf.au aims to be an inclusive event which welcomes diverse groups from all parts of the FLOSS (Free, Libre, Open Source Software) community to an environment of respect, tolerance, and encouragement.
linux.conf.au has an enforced [Code of Conduct](/attend/code-of-conduct/), with all complaints treated confidentially and seriously.

## Sponsorship Packages

All sponsors are acknowledged each day of the conference during the opening session.
If you are interested in contributing to making linux.conf.au 2021 a success, please reach out for more information.

### Emperor Penguin

Emperor Penguin is our highest level of sponsorship.

Sponsorship package includes:
* Identification as an Emperor Penguin sponsor of the conference
* Ten Professional Tickets
* Four additional tickets at the early bird price
* Three additional PDNS-only tickets to permit additional representation at the PDNS
* An opportunity to address the professional delegates during the PDNS
* Promotion via banners across the keynote event, conference streaming platform, title and end slides of sessions, waiting room slides, at the PDNS, and mentions via our numerous communication channels to our delegates, mailing list and followers
* Promotion of your logo prominently on the conference website and every delegate badge

### King Penguin

Sponsorship package includes:
* Identification as a King Penguin sponsor of the conference
* Eight Professional Tickets
* Three additional tickets at the early bird price
* Two additional PDNS-only tickets to permit additional representation at the PDNS
* Promotion via banners across the PDNS stream, waiting room slides and during the conference via the streaming platform
* Promotion of your logo on the conference website sponsors page

### Royal Penguin

Sponsorship package includes:
* Identification as a Royal Penguin sponsor of the conference
* Six Professional Tickets
* Up to two additional tickets at the early bird price
* Two additional PDNS-only tickets to permit additional representation at the PDNS
* Promotion via logo mentions during the daily opening
* Promotion of your logo on the conference website sponsors page

### Adelie Penguin

Sponsorship package includes:
* Identified as an Adelie Penguin sponsor of the conference
* Four Professional Tickets
* One additional ticket at the early bird price
* One additional PDNS-only ticket to permit additional representation at the PDNS
* Promotion via logo mentions during daily opening
* Promotion of your logo on the conference website sponsors page

### Fairy Penguin

Sponsorship package includes:
* Identified as a Fairy Penguin sponsor of the conference
* One Professional Ticket
* Promotion of your logo on the conference website sponsors page
* Entry level financial sponsor for companies who wish to provide support to the conference

## Additional Sponsorship Opportunities

Conference sponsors have the opportunity to contribute in a more targeted way on top of their general sponsorship.
There are a number of additional packages available, each with limited availability.

### Lanyard Sponsor

A highly sought-after opportunity!
The first 200 registered and paid delegates will receive a Schwag/Delegate Pack which will include a lanyard/badge.
Many delegates collect lanyards and reuse them time and time again.
Your branding, along with linux.conf.au branding, will be on the lanyard, providing a high level of visibility for the associated sponsor.

### Video Sponsor

The sponsor will be introduced at the conference as having funded video streaming to delegates.
Talks will be uploaded for later viewing by delegates and our wider community, and the video sponsor will have their logo included in the title slide.

### Schwag/Delegate Packs Sponsor

For linux.conf.au 2021 we are looking to send out conference schwag to our first 200 delegates.
By adding on this sponsorship package, the sponsor will be able to place their logo onto the packaging and are able to place a small item into the package (space permitting).

### Speaker's Pack Sponsor

Contribute directly to improving the quality of our speakers' talks.
In the Speaker's Packs we are hoping to include Audio/Visual equipment where possible to ensure the best quality conference possible for everyone.
Speaker's pack sponsors will be noted in the pack that is provided to the speaker.

### Outreach and Inclusion Sponsor

Contribute directly to increasing the diversity of the free and open source technology community by sponsoring our Outreach and Inclusion Programme.
This programme reaches out to diverse delegates, from a variety of backgrounds, throughout the community.
Outreach and inclusion sponsorship supports delegates from diverse backgrounds who have a significant contribution to make, but would otherwise be unable to attend linux.conf.au.

### Miniconf Sponsor
A miniconf is a single day track with talks dedicated to a specific topic.
Sponsors can choose to add-on a miniconf sponsorship package, giving them exclusive branding rights with their logo visible alongside the talks during the day, as well as co-located with the video sponsor on videos for talks within that miniconf.
Each miniconf can have a single sponsor, making this a very limited opportunity.

### Catering Sponsor

Contribute directly to the catering of an Online conference.
We would love to hear your ideas on what or how this may look.

### In-Kind Sponsorship

Organisations that make a substantial material contribution to the organisation of the conference are entitled to be listed as in-kind sponsors of the conference.
In-kind sponsors will be noted at the opening and closing plenary sessions of the conference, and will have their logo on the Sponsors page on the conference website.
Potential opportunities for in-kind sponsorship include:

* Funding the attendance and registration costs of a speaker invited to the conference.
* Providing equipment necessary to successfully run conference events.
* Donating prizes for giveaways at conference plenaries and social events.
* Providing collateral for the Conference delegate packs.

Benefits of in-kind sponsorship will be assessed on a case-by-case basis.
We'd love to hear about what you have in mind, just email [sponsorship@lca2021.linux.org.au](mailto:sponsorship@lca2021.linux.org.au).
