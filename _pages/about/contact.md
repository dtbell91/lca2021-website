---
layout: page
title: Contact
permalink: /about/contact/
sponsors: true
---

## Contact the Organising Team

Is there something you would like to know that isn't on this site? We have multiple contact methods, with email being preferred.

* Email: [contact@lca2021.linux.org.au](mailto:contact@lca2021.linux.org.au)
* Twitter: [@linuxconfau](https://twitter.com/linuxconfau), hashtag #lca2021
* Facebook: [@linuxconferenceaustralia](https://facebook.com/linuxconferenceaustralia)
* LinkedIn: [linuxconfau](https://www.linkedin.com/company/linuxconfau/)
* Announce mailing list: [LCA Announce](https://lists.linux.org.au/mailman/listinfo/lca-announce)

## Contact other organisations

* Linux Australia
  * Auspice for linux.conf.au
  * Website: [linux.org.au](https://linux.org.au)

## Chat with attendees

* IRC (unofficial): #linux.conf.au on freenode
* Chat mailing list: Please subscribe via your [Dashboard](/dashboard/) and wait for changes to be synchronised
