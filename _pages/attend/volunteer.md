---
layout: page
title: Volunteer
permalink: /attend/volunteer/
sponsors: true
---

linux.conf.au is a grass-roots, community-driven event.
It needs bright, enthusiastic people - like you - to make it a success!

Volunteering at linux.conf.au is a great opportunity and can be deeply rewarding.
On a more practical note, volunteering at linux.conf.au can add to your professional portfolio in many ways, making this an ideal opportunity for students or recent graduates.

By volunteering at linux.conf.au, you will have the benefit of:

* Meeting exceptional people - many of whom are recognised experts in their field and industry luminaries
* Great networking opportunities with people working in the free and open source community
* Gaining practical, hands-on experience and skills in event management, customer liaison, public relations and operation of audio visual equipment - which are invaluable to future employers

## What's required of me?
To be eligible for Volunteer Registration at linux.conf.au you need to:

* be willing to commit to three full days of volunteering
* able to attend the mandatory training and induction prior to the conference

The following is a indication of the tasks available across the conference:

<div class="table-responsive">
<table class="table table-striped table-bordered" summary="This table provides a summary of the days volunteers will be needed">
  <tr>
    <th>Day</th>
    <th>Tasks available</th>
  </tr>
  <tr>
    <td>Saturday 23 Jan</td>
    <td>Miniconf room monitoring, AV and general assistance</td>
  </tr>
  <tr>
    <td>Sunday 24 Jan </td>
    <td>Conference room monitoring, AV and general assistance</td>
  </tr>
  <tr>
    <td>Monday 25 Jan</td>
    <td>Conference room monitoring, AV and general assistance</td>
  </tr>
</table>
</div>

**We would also like you to have one or more of the following skills and/or aptitudes:**

* the ability to meet and greet people and provide assistance with a friendly, cheerful disposition
* some familiarity with audio visual technologies
* confidence addressing an audience, such as introducing a speaker
* some familiarity with technology such as IRC, social media, text editors, etc

**The types of tasks you can be expected to be assigned to include:**

* Operating an audio visual setup to stream the conference to our delegates
* Assisting delegates and ensuring questions related to the conference are answered
* Introducing speakers and ensuring they run to time
* Room monitoring activities including co-ordinating with other conference rooms
* General event administration tasks

## What's in it for me?

Tempted, but want to know more about what you get for volunteering your time?
This is what we're offering:

* Free conference registration at Hobbyist level
* Written reference covering your experience and duties - useful for job interviews!

## Sign me up!

<a href="https://forms.gle/uJVd1YGGPT7vYzUm9" class="btn btn-outline-primary" role="button" target="_blank">Apply to Volunteer</a>

Any questions? Email us at [volunteers@lca2021.linux.org.au](mailto:volunteers@lca2021.linux.org.au)
