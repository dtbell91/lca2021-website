---
layout: page
title: Tickets
permalink: /attend/tickets/
card: tickets_open.5ca3bc19.png
sponsors: true
---

## Ticket Information

### Purchase your ticket

To purchase a ticket, please login to your [Dashboard](/dashboard/).
Once you are logged in you will have an option to purchase a ticket on the dashboard page.
Payment is by Visa, Mastercard, or American Express cards only.

<a href="/dashboard/" class="btn btn-outline-primary" role="button">Go to Dashboard</a>

If you have any special requirements or concerns that are not captured in our registration process please don't hesitate to get in touch with us.

### Key Dates

 * Registrations Open: 23 November 2020
 * Conference Opens: 23 January 2021

### Attendee Expectations

All attendees of the conference must agree to abide by the conference's [Code of Conduct](/attend/code-of-conduct/).
The conference aims to provide a safe and welcoming environment for all attendees and we take the Code of Conduct seriously.

All ticket purchases are subject to the conference [Terms and Conditions](/attend/terms-and-conditions/).

## Prices and inclusions

All prices quoted are in Australian Dollars and include 10% Australian Goods and Services Tax (GST).

{% include ticket_matrix.html %}

## Ticket types

### Contributor

The Contributor ticket is designed for those who wish to attend linux.conf.au as a Professional delegate, as well as supporting the conference by contributing financially.
In recognition of this support, Contributors have the option to get their logo and a link to their organisation published on the Contributors page.

### Professional

The Professional ticket is the standard full inclusion conference ticket.
This rate applies to most people who have their companies pay the conference fees, or for individuals who can otherwise afford to support the conference at this level.

We ask that you choose this ticket if your company is paying for you to attend.

### Hobbyist

The Hobbyist rate is heavily discounted for open source enthusiasts who are paying out of their own pockets and would otherwise find it difficult to attend.

### Student

This is a concession rate ticket that is reserved for High School, College, TAFE or University Students.
linux.conf.au offers this rate as a form of investment in the future of the free and open source software community.
As part of the registration process, a valid student ID card or proof of enrolment must be presented to the conference organisers via [email](mailto:contact@lca2021.linux.org.au).
Any Student who cannot provide this will be required to register as a hobbyist by paying the difference in fees between the Student rate and the Hobbyist rate.

### Miniconf Only

This ticket gives access to the Miniconf day of the conference only.
Delegates with this ticket level will be able to see talks at any of the [Miniconfs](/programme/miniconfs/), but will not have access to the main conference.

## Other information

### Volunteers

linux.conf.au would not be as successful as it is without having a dedicated team of volunteers to help run the conference.
If you're interested in spending the week of linux.conf.au 2021 helping run the conference, please [apply to be a volunteer](/attend/volunteer/).

### Media

There are a limited number of Media Passes available to media personnel.
Media Passes are free of charge, and entitle media personnel to attend linux.conf.au with all the entitlements of a Professional registration.
Please note, due to the limited numbers of Media Passes available, all Media Passes will need to be approved by the organising team.

To apply for a Media Pass, please [contact our team](mailto:media@lca2021.linux.org.au).

### Go Green and Carbon Offset

When purchasing your ticket there is an option to purchase carbon offsets.
