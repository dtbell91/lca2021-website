---
layout: page
title: Financial Assistance
permalink: /attend/assistance/
sponsors: true
card: financial_assistance.3fd3459b.png
---

## Do you need financial assistance to attend linux.conf.au 2021?

Thanks to the support of our Outreach and Inclusion sponsor, [Arm](/sponsors/), we have funds set aside to provide financial assistance to linux.conf.au attendees who might need it.
This program is part of our outreach and inclusion efforts for linux.conf.au.

Anyone can apply for financial assistance to attend linux.conf.au 2021.
You can apply for full or partial financial assistance to cover the costs of what you might need to join the conference.
You could include things like:

* Your conference ticket (at the hobbyist or student level)
* Additional internet bandwidth
* Technology equipment
* Other costs associated with attending, such as a support worker or child care.

Please ask only for what you need to attend the conference.
We will be processing applications weekly until funds are exhausted, so we encourage you to apply as soon as you can for a greater chance of being accepted.

## How to apply

To apply for financial aid, please send an email to [assistance@lca2021.linux.org.au](mailto:assistance@lca2021.linux.org.au) providing details around the financial aid you would like to receive.
Please include the total amount you are requesting in Australian Dollars (AUD), not including tickets (if you are only requesting tickets state $0 requested).

In your email, please tell us all the awesome things you'll get out of attending linux.conf.au and how you'll share your experiences with others.

Please also let us know if you’ve received financial aid to attend a previous linux.conf.au (this is for our records, it will not be used to assess your application).

## Selection criteria

In addition to allocating financial aid based on need, we also consider how we can get the most benefit for all linux.conf.au attendees and the wider community.

In your email, please mention if you:

* Can help linux.conf.au achieve our goal of bringing together of a diverse set of backgrounds and perspectives
* Are a volunteer or helping in another way to make the conference more awesome
* Are a Miniconf speaker (or have applied to be one)
* Are a member or organiser of an open source or related group and would share your linux.conf.au experience with the community
* Are a teacher or other educator and attending linux.conf.au would benefit students
* Are a maintainer or contributor to an open source project
* Identify as being in any under-represented groups

Note that these are factors we consider, but they are not a checklist.
You do not need to have any of the above to be eligible for financial aid.

## Reimbursement process

If granted assistance, you will still be expected to organise and purchase everything you need yourself.
You will receive a maximum amount of reimbursement based on your request.
Once we've granted assistance, you can send us your receipts for reimbursement.

If you have any questions about financial assistance please contact us at [assistance@lca2021.linux.org.au](mailto:assistance@lca2021.linux.org.au).
