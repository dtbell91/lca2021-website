---
layout: page
title: Shirts and Swag
permalink: /attend/shirts/
sponsors: true
---

## SwagBadges

Have you heard about our awesome conference SwagBadges?
We have 200 available (Australia only, sorry!) which we'll be posting out in December.
It's first in, best dressed, so if you'd like to score one of these cool gadgets you'll need to book your conference ticket before December 1 2020.

A big thank you to our [Open Hardware Miniconf](/programme/miniconfs/open-hardware/) team who have been busy building these over the past few months.
Check out the [SwagBadge website](http://www.openhardwareconf.org/wiki/Swagbadge2021) for more information, including the full designs and details on how you can create your own!

## Shirts

Due to linux.conf.au 2021 being online, we are not offering shirts with our standard tickets.

But do not fear!
We know that many of you would still like to get a memento of attending LCA2021, so we have setup an online store for you to purchase shirts and other items (coffee mugs, shower curtains, etc).

<a href="https://www.redbubble.com/shop/ap/61494974" class="btn btn-outline-primary" role="button">Buy Shirts and Swag</a>
