---
layout: page
title: Information for Miniconf Organisers
permalink: /programme/miniconf-organiser/
sponsors: true
---

## Overview

Miniconf organisers are responsible for planning an running a day-long specialist track at linux.conf.au.
The LCA organisers will assist with many parts of the process to make the job simpler.

## Call for Miniconfs

The Call for Miniconfs is now closed.
Thank you to everyone who submitted a proposal.

 * Call for Proposals Opens: 15 October 2020
 * Call for Proposals Closes: 6 November 2020 [Anywhere on Earth (AoE)](https://en.wikipedia.org/wiki/Anywhere_on_Earth)
 * First round of acceptances: starting 23 November 2020
 * Conference Opens: 23 January 2021

If you would like to review your Miniconf proposals, you can continue to do so via your [Dashboard](/dashboard/).

<a href="/dashboard/" class="btn btn-outline-primary" role="button">Go to Dashboard</a>

## What is expected from a Miniconf organiser?

Miniconf organisers will need to arrange speakers to fill the schedule for their allocated day and room.
The conference team will provide a call for proposals system on the conference website for people to submit proposals for each Miniconf.
The proposal submission form can be adjusted per Miniconf as required, for example to ask additional questions around presentation format.
Miniconf organisers can then review the proposals for their Miniconf and accept sessions they would like to appear.
Once all session acceptances have been made, the Miniconf organiser will need to arrange the talks into a schedule for the day and provide this to the conference team for it to be published on the website.

Each accepted Miniconf will be allocated a number of tickets to assign to their speakers as they deem appropriate.
These tickets will be applied to the nominated accepted speakers via the conference ticketing software.
Note that this may not cover all speakers for a Miniconf, depending on the number of accepted speakers.
All participants in your Miniconf must have a valid ticket to the conference or they will not be able to attend.
Further details of this will be provided to each Miniconf organiser as part of their acceptance.

## What has changed with the move to online?

As you can expect there have been quite a few changes to linux.conf.au 2021 due to being online.
To reduce complexity and to minimise risk, we will be moving to having all Miniconfs follow the schedule times.
The well-being of our attendees, volunteers and organising team is essential for us.
To be able to provide a comfortable experience for our attendees we require Miniconfs to ensure that the scheduled breaks are followed to allow our attendees to stand-up, rest, walk away from their computers (this also ensures your speakers get a break as well).

The schedule will follow the template below:

{% include schedule.html %}
