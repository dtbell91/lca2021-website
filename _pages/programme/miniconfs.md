---
layout: page
title: Miniconfs
permalink: /programme/miniconfs/
card: miniconfs.4d943c68.png
sponsors: true
---

## What are Miniconfs?

The first day of linux.conf.au is made up of dedicated day-long streams focussing on single topics.
They are organised and run by volunteers and are a great way to kick the week off.

First introduced at linux.conf.au 2002, they are now a traditional element of the conference.
They were originally intended as an incubator -- both of future conferences and speakers.

## Call for Sessions

**Our call for miniconf sessions is now closed.**
Thank you to everyone who submitted a proposal to our miniconfs.

If you would like to review your proposals, you can continue to do so via your [Dashboard](/dashboard/).

<a href="/dashboard/" class="btn btn-outline-primary" role="button">Go to Dashboard</a>

## Miniconfs at linux.conf.au 2021

{% include miniconf_grid.html %}

## Information for Miniconf Organisers

Are you a Miniconf organiser, or would you like more information about what is involved with running a Miniconf?
Read our [Miniconf Organiser](/programme/miniconf-organiser/) page for more information.
