---
layout: page
title: Programme Overview
permalink: /programme/
sponsors: true
---

The conference starts out with one day of miniconfs followed by two days of sessions on our main program.

While the conference is a virtual event and available around the world, it is important to note that we will be using Australian Eastern Daylight Time (AEDT - UTC+11) for our sessions.

## Week Overview

<div class="table-responsive">
  <table class="table table-striped table-bordered" summary="This table provides a summary of the conference days">
    <tr>
      <th>Day</th>
      <th>Start of Day</th>
      <th>Daytime</th>
      <th>Evening</th>
    </tr>
    <tr>
      <th>Saturday</th>
      <td>Conference Welcome<br>Keynote</td>
      <td>Miniconfs</td>
      <td></td>
    </tr>
    <tr>
      <th>Sunday</th>
      <td>Keynote</td>
      <td>Main Conference</td>
      <td></td>
    </tr>
    <tr>
      <th>Monday</th>
      <td>Keynote</td>
      <td>Main Conference <br> Conference Close</td>
      <td></td>
    </tr>
  </table>
</div>

## Daily Schedule Overview

{% include schedule.html %}

## Presentation Types

### Welcome Address
The conference organisers start the event by welcoming everyone and letting everyone know the general housekeeping requirements of the conference.

### Keynotes
Each conference morning will begin with a keynote presentation.
We are really excited by our invited guests this year and will be making announcements very soon.

### Talks
Talks are 35-45 minute presentations on a single topic.
These are generally presented in lecture format and form the bulk of the available conference slots.

### Conference Close
The final part of the conference will include a thanks to all volunteers, announcement of the next year's LCA location and formal closing.
