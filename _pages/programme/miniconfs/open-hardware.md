---
layout: page
title: Open Hardware Miniconf
permalink: /programme/miniconfs/open-hardware/
sponsors: true
---

<p class="lead">
Organised by Jonathan Oxer and Andy Gelme
</p>

## When

Saturday 23 January 2021

## Call for Sessions

{% include miniconf_cfs.md miniconf_slug="open-hardware-miniconf" day="saturday" closed="True" %}

## About

Were you fast enough on the LCA2021 registration button to score yourself a Swag badge with your ticket?
Come find out more about the shiny hardware, what you can do with it right now and how you might like to extend it in the future!
If you missed out on a Swag badge, we also have instructions for how you can order components and make your own Dag badge from scratch.

The concept of Free / Open Source Software is complemented by a strong community focused around Open Hardware and the "maker culture" who often create their own products or HackerSpaces.
One of the drivers of the popularity of the Open Hardware community is easy access to cheap and capable devices such as the ESP32 and the Arduino, which is a microcontroller development board originally intended for classroom use, but now a popular building block in all sorts of weird and wonderful hobbyist and professional projects.

Interest in Open Hardware continues to increase amongst FOSS enthusiasts, but there is also a barrier to entry with the perceived difficulty of working with unfamiliar electronics and the more constrained firmware development environments.
The Open Hardware Miniconf will ease people into dealing with hardware.
Presentation topics will cover both software and hardware issues, starting with simpler sessions suitable for Open Hardware beginners and progressing through to more advanced topics.

Due to LCA2021 being online, we won't be able to perform our usual hardware build workshop and expose newcomers to hot soldering irons, etc.
Instead we'll focus on:

* Quick start on having fun with your shiny new Swag badge / Dag badge
* Swag badge / Dag badge presentations covering hardware, firmware and applications
* Open Hardware presentations by LCA2021 attendees based on the usual LCA Call for Sessions process
* Introduction to electronics
* Introduction to firmware, primarily microPython and C
* Opening up a potential OHMC2022 to a broader, more diverse group of contributors and projects
* Finish with attendee Swag badge / Dag badge show-and-tell, effectively Lightning Talks

The day will run in two distinct halves.

The morning sessions will discuss the design of the LCA2021 Swag badge, explaining the hardware and firmware that runs on it.
Issues covered will include the engineering tradeoffs when creating a project with minimal risk on a limited timeframe and budget with unpredictable logistics.

The Simple Add-On standard will be explained, to provide participants with a starting point for building their own extensions to the conference badge.

We'll also discuss the Dag badge and the unique requirements and thought processes that went into its use and design, and how it relates to the Swag badge.

The afternoon session will be presentations about more general Open Hardware topics, with contributions by both LCA2021 attendees and the OHMC team.

The Swag badge and Dag badge are being fully documented at [http://www.openhardwareconf.org](http://www.openhardwareconf.org)
