---
layout: page
title: GO GLAM Miniconf
permalink: /programme/miniconfs/glam/
sponsors: true
---

<p class="lead">
Organised by Hugh Rundle and Bonnie Wildie
</p>

## When

Saturday 23 January 2021

## Call for Sessions

{% include miniconf_cfs.md miniconf_slug="glam-miniconf" day="saturday" closed="True" %}

## About

This Miniconf will explore what's next for open culture and memory.

As COVID-19 swept across the planet, advocates for open access to academic and cultural knowledge finally had their moment in the sun, as GLAM workers have scrambled to provide greater access to digital and digitised collections for students and researchers.
Open data about COVID-19 is being shared and tracked globally in real time in an unprecedented effort, and cultural institutions have pivoted to online, providing a steady stream of openly available educational blogs, videos, home-schooling packs, and in one memorable case, even an unexpectedly popular synth-pop song.
Academic, professional and community conferences - like linux.conf.au itself - have rapidly moved from expensive events based in a single physical and temporal space to become global, online and often asynchronous.

At the same time, governments are tightening control of the official archives, reducing funding for cultural and memory institutions, expanding confidentiality provisions that obscure decision making, and Australian universities are shedding thousands of staff, including dozens of library and technology staff even as digital learning support needs expand.
COVID-19 has seen galleries, archives, libraries and museums (GLAM) around the world close their doors - in some cases permanently.

And under the cover of all this chaos, companies and government are destroying some of the world's most ancient and precious cultural sites - whether it be Rio Tinto blowing up an ancient sacred rock cave at Jukaan Gorge, the Victorian government felling birthing trees in Djab Wurrung country to build a highway, or Coastal GasLink and the Canadian Police forces building a pipeline across unceded Wet'suwet'en territories.

So what's next?

What should GLAM workers be thinking about, doing, and supporting?
What should FOSS technologists help to build and maintain?
How can open knowledge advocates add the most value in our current moment?
How can we use open and free technologies to support communities to maintain strong and living cultures, and defend open and transparent government, science and societies?

We invite you to share a project you're working on, a proposal for a new project, a call to action, a history lesson - or any other presentation that can help to answer "So what's next?" and help us all to get there.
