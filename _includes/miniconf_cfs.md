{% if include.closed %}
  Submissions for this Miniconf are now closed.

  <a class="btn btn-md btn-outline-primary" href="/schedule/#{{ include.day }}">View the schedule</a>
{% else %}
  {{ include.lead | default: "The call for sessions is now open!" }}

  * Call for Sessions Opens: 23 November 2020
  * Call for Sessions Closes: 18 December 2020 [Anywhere on Earth (AoE)](https://en.wikipedia.org/wiki/Anywhere_on_Earth)
  * Conference Opens: 23 January 2021

  {% if include.miniconf_slug %}
  <a class="btn btn-md btn-outline-primary" href="/proposals/submit/{{ include.miniconf_slug }}/">Submit a proposal</a>
  {% endif %}
{% endif %}
