---
layout: post
title: Call for Sessions and Miniconfs now open!
card: sessions.0b1adf3f.png
---

<p class="lead">We're excited! The linux.conf.au 2021 Call for Sessions and Call for Miniconfs are now open. They will stay open until 6 November 2020 <a href="https://en.wikipedia.org/wiki/Anywhere_on_Earth">Anywhere on Earth (AoE)</a>.</p>

Our theme is "So what's next?".

We all know we're living through unprecedented change and uncertain times.
How can open source play a role in creating, helping and adapting to this ongoing change?
What new developments in software and coding can we look forward to in 2021 and beyond?

If you have ideas or developments you'd like to share with the open source community at linux.conf.au, we'd love to hear from you.

## Call for Sessions

The main conference runs on Sunday 24 and Monday 25 January, with multiple streams catering for a wide range of interest areas.

We invite you to submit a [session](/programme/sessions/) proposal for a talk.
Talks are generally 35-45 minute presentations on a single topic presented in lecture format.

## Call for Miniconfs

Miniconfs are dedicated day-long streams focusing on single topics, creating a more immersive experience for delegates than a session.
We encourage you to get creative with how you could deliver your Miniconf virtually!

Running a [Miniconf](/programme/miniconfs/) is a great way to gain experience, provide exposure for your project or topic, and raise your professional profile.
They're a crowd favourite and an awesome way to kick off the conference.

Miniconfs will run on Saturday 23 January, before the main conference commences on Sunday.

## No need to book flights or hotels

Don't forget: the conference will be a fully online, virtual experience.
This means our speakers will be beaming in from their own homes or workplaces.
The organising team will be able to help speakers with their tech set-up.
Each accepted presenter will have a tech check prior to the event to smooth out any difficulties and there will be an option to pre-record presentations.

## Have we piqued your interest?

You can find out how to submit your session or miniconf proposals [here](/programme/proposals/).
If you have any other questions you can contact us via [email](mailto:contact@lca2021.linux.org.au).

We're looking forward to reading your submissions.

linux.conf.au 2021 Organising Team
