---
layout: post
title: Keynote Announcement - Omoju Miller
card: keynote_omoju.768e6223.png
---

<p class="lead">The good news keeps on coming! We're delighted to announce that Omoju Miller will be one of our keynote speakers at linux.conf.au 2021.</p>

<img class="img-fluid float-sm-right profile-pic" src="/media/img/omoju_miller.jpg" width="250">

In her keynote, Omoju will discuss participation in open source as a form of cognitive apprenticeship.
She will map out how the transition step from novice to expert aligns with the progressions we see in the roles taken on in open source communities.

Omoju will share ideas on how to make the transition from beginner to master faster by accessing what we know from education research.

This follows the earlier announcement of our first keynote speaker, Limor Fried.

Tickets are now available at linux.conf.au. Secure yours today to hear what Omoju and Limor have to say.

## About Omoju Miller

Omoju Miller is the Technical Advisor to the CEO at GitHub.
Her expertise is in the area of machine learning and computational intelligence.

Previously, Omoju co-led Google's non-profit investment in computer science education and served as a volunteer advisor to the Obama administration's White House Presidential Innovation Fellows.

Omoju joined Github in 2017 to help build recommendation engines for the popular open-source software development platform.
She took on the role of Technical Advisor to the CEO in February 2020.
Originally from Lagos, Nigeria, Omoju holds a doctoral degree in Computer Science Education from UC Berkeley.

We look forward to welcoming Omoju to linux.conf.au 2021.

## About linux.conf.au

Australasia's grassroots Free and Open Source technologies conference, linux.conf.au, will be held online, worldwide, from Saturday 23 to Monday 25 January 2021.
Running since 1999, linux.conf.au is the largest linux and open source conference in the Asia-Pacific region.
The conference provides deeply technical presentations from industry leaders and experts on a wide array of subjects relating to open source projects, data and open government and community engagement.

## How to get tickets?

Jump onto linux.conf.au to grab your ticket via your [dashboard](/dashboard/).

If you need help convincing your employer to let you attend linux.conf.au 2021, we have a page to assist.
Check out our [why should your employees attend](/attend/why-should-employees-attend/) page, and share it with your employer.

## Have you seen our schedule yet? Have you noticed the room names?

Each of the [rooms](/schedule/) are dedicated to a person that has had a great impact on linux.conf.au and Linux Australia.

**Tux Theatre** - is named after Linux's mascot Tux (duh) but because we couldn't be in [Tux's hometown](https://upload.wikimedia.org/wikipedia/commons/1/17/TheStoryBehindTux.jpg) Canberra for 2021 we thought we would dedicate the main theatre to Tux!

**Rusty R. Hall** - named after [Paul "Rusty" Russell](https://en.wikipedia.org/wiki/Rusty_Russell).
He was the one who started all of this linux.conf.au mess when he organised the Conference of Australian Linux Users (CALU) in 1999, and which then helped initiate Linux Australia.

**Pia Andrews Conservatory** - [Pia Andrews](https://en.wikipedia.org/wiki/Pia_Andrews) is best known for her open government work and her years of contributions to Linux Australia.
Pia is also a musician and a Conservatory in musical terms is a place for learning music so we thought calling the stream a Conservatory would be perfect!

**Blemings Labs** - Now for those of you who doesn't know Hugh (turns out he doesn't have a Wikipedia page, someone please fix that!), Hugh has been an attendee at linux.conf.au at most, if not all, prior LCAs and is a previous Linux Australia President and Council Member.
