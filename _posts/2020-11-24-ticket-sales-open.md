---
layout: post
title: Ticket Sales now open!
card: tickets_open.5ca3bc19.png
---

<p class="lead">We are pleased to announce that ticket sales for linux.conf.au 2021 have now launched!</p>

This year the conference is taking place online, worldwide, from Saturday 23 to Monday 25 January.
Saturday 23 January will be a day of Miniconfs, followed by two days of main conference on Sunday 24 and Monday 25 January.

The online format means we can offer tickets at a lower price to previous years.
This does mean that there are no Early Bird tickets.
Jump onto your [dashboard](/dashboard/) to secure yours today.

## Prices
* Miniconf Only (Saturday only pass): $25
* Student/Concession: $30
* Hobbyist: $70
* Professional: $125
* Contributor: $300

For full details and inclusions, take a look at the [tickets](/attend/tickets/) page.
All prices are AUD, including GST.

## Swag badges on offer for first 200 registrants

Have you heard about our awesome conference [swag badges](/attend/shirts/)?
We have 200 available (Australia only, sorry!) which we'll be posting out in December.
It's first in, best dressed, so if you'd like to score one of these cool gadgets you'll need to book your conference ticket before December 1 2020.

## Pick your own LCA2021 branded merchandise

If you've attended linux.conf.au in the past you know we usually hand out branded t-shirts.
Since we've moved online for 2021 we can't do this, so we wanted to produce something special.
This year you can get the logo placed on a whole bunch of items through [our RedBubble store](https://www.redbubble.com/shop/ap/61494974).
You can buy branded phone cases, face masks, t-shirts, socks and lots more.
(Sae Ra, our conference chair, really wants someone to buy the shower curtain!)

## Volunteering

If you are keen to volunteer at the conference you can apply today.
As linux.conf.au is a community-focused, grassroots conference, it requires the support of volunteers.
Please see the [volunteers page](/attend/volunteer/) for full details.

But wait... there's more! (and no it's not steak knives!)

## We are also announcing Miniconfs!

We are so pleased to announce that we have decided on our Miniconfs for this year.
This was a VERY competitive year as we only have 4 slots available and we received many more submissions.
We really wished that we could accept all of our Miniconfs but due to the shorter conference schedule we haven't been able to accommodate.

Your [Miniconfs](/programme/miniconfs/) for 2021 are:

* [Open Hardware](/programme/miniconfs/open-hardware/)
* [Kernel](/programme/miniconfs/kernel/)
* [Systems Administration](/programme/miniconfs/system-administration/)
* [GO GLAM](/programme/miniconfs/glam/)

Their Call for Miniconf Sessions is open now!
This is also available via your [Dashboard](/dashboard/).
We are really looking forward to seeing some great talks at Miniconfs this year.

If you have any questions about ticket sales or anything to do with linux.conf.au 2021, you can contact us via [email](mailto:contact@lca2021.linux.org.au).
