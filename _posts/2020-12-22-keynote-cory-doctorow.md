---
layout: post
title: Keynote Announcement - Cory Doctorow
card: keynote_cory.c3e299dd.png
---

<p class="lead">Exciting news! We're thrilled to announce that Cory Doctorow will be one of our keynote speakers at linux.conf.au 2021.</p>

<img class="img-fluid float-sm-right profile-pic" src="/media/img/cory_doctorow.jpg" width="300">

Be sure to grab a ticket to hear Cory explain how software freedom is essential to human freedom, and discuss the urgency of reclaiming our digital rights in an era of digital monopolies.

## About Cory Doctorow

[Cory Doctorow](https://craphound.com) is a science fiction novelist, journalist and technology activist.
He's a special consultant to the [Electronic Frontier Foundation](https://eff.org), a non-profit civil liberties group that defends freedom in technology law, policy, standards and treaties.
He holds an honorary doctorate in computer science from the Open University (UK), where he is a Visiting Professor; he is also a MIT Media Lab Research Affiliate and a Visiting Professor of Practice at the University of North Carolina's School of Library and Information Science.

Cory is the award-winning author of _Walkaway_, _Little Brother_, and _Information Doesn't Want to Be Free_ (among many others).
His latest book released earlier this year is _Attack Surface_, a standalone adult sequel to _Little Brother_.
His novels have been translated into dozens of languages.

Cory co-founded the open source peer-to-peer software company OpenCola.
He is an activist in favour of liberalising copyright laws and a proponent of the Creative Commons organisation, using some of their licenses for his books.

In his speeches, Cory Doctorow outlines key issues of the digital age - in particular, the confluence of intellectual property law, futurism and politics.
According to Cory, digital copyright laws have, in general, not served either creators or users well since the dawn of the internet.

You can follow Cory's blog at [Pluralistic.net](https://pluralistic.net).

We are looking forward to welcoming Cory to linux.conf.au 2021.

## About linux.conf.au

Australasia's grassroots Free and Open Source technologies conference, linux.conf.au, will be held online, worldwide, from Saturday 23 to Monday 25 January 2021.
Running since 1999, linux.conf.au is the largest linux and open source conference in the Asia-Pacific region.
The conference provides deeply technical presentations from industry leaders and experts on a wide array of subjects relating to open source projects, data and open government and community engagement.

## How to get tickets?

Jump onto linux.conf.au to grab your ticket via your [dashboard](/dashboard/).

If you need help convincing your employer to let you attend linux.conf.au 2021, we have a page to assist.
Check out our [why should your employees attend](/attend/why-should-employees-attend/) page, and share it with your employer.

## Have you received your SwagBadge yet?

Our awesome Open Hardware team have been building the LCA2021 [SwagBadge](/attend/swag-badge/) for the past few months.
They reached an exciting milestone this week, starting to send out badges to delegates!

We wanted to send a very special shout out to our SwagBadge team: Jon Oxer, Andy Gelme, Nicola Nye, John Spencer and Andrew Nielsen.
Without them all those parcels of goodies wouldn't have made it to our attendees.
There are still some badges left, so if you're in Australia make sure you [grab a ticket](/dashboard/) today and choose to get a SwagBadge!
