---
layout: post
title: Help to Deliver linux.conf.au 2021
card: team_eoi.558234c3.png
---
<p class="lead">Expressions of interest for Core Team and Volunteers are now open.</p>

We are now looking for volunteers.
linux.conf.au wouldn't be the awesome conference it is if it weren't for volunteers!
For 2021 we're calling on people to come forward and lend a hand again.
If you've helped run a Linux Australia Conference before and just want to get on with signing up, you can fill out the [expression of interest form](https://forms.gle/uJVd1YGGPT7vYzUm9) now!

If you're new to it all and want to help out, keep reading!

We're all volunteers ourselves and we need more – we can't run the conference by ourselves, particularly during the week of the conference itself.

In the lead up to 2021 we need a core team consisting of:
* Speaker Liaison
* Sponsor Liaison
* Volunteer Liaison
* Money Handler
* Schwag Wrangler
* Media Powah
* Keynote Organiser
* Tech Wrangler

During the Conference we will need help with:
* Room monitoring
* AV
* Chat monitoring
* Maybe catering? Yes, we know it's virtual but we have so many ideas!

Anyone who has volunteered before will tell you it's a very busy time, but also very worthwhile.
It's satisfying to know that you've helped everyone at the conference to get the most out of it.
It's very rewarding to know that you've made a positive difference to someone's day.

You don't just get to meet the delegates and speakers, you also get to know many of them while helping them out online.
You will be presented with a very unique opportunity to get behind the scenes of a different look LCA and help make history as we bring this wonderful conference online.
While interacting with speakers and delegates online you will forge new relationships with amazing, interesting and wonderful people (just like you) whom you might not have otherwise had the good fortune to meet in any other way.

In return for your help we'll provide you with:
* Special Volunteer Schwag
* If you want one, a letter of reference at the end of the week

Depending on the number of volunteers we get and workload (many hands make light work), we will make sure you get the best experience possible as you help us put this together.

For more information, please check out please don't hesitate to contact me at [president@linux.org.au](mailto:president@linux.org.au) – we are still working on the finer details of what we need from our volunteers but we are happy to field any questions that you may have.
We review and approve applications regularly.

Sae Ra Germaine<br>
President<br>
Linux Australia
