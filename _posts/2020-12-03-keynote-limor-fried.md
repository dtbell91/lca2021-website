---
layout: post
title: Keynote Announcement - Limor Fried
card: keynote_limor.f7b973a0.png
---

<p class="lead">We're delighted to announce that Limor "Ladyada" Fried, electrical engineer and founder of Adafruit, will be one of our keynote speakers at linux.conf.au 2021.</p>

<img class="img-fluid float-sm-right profile-pic" src="/media/img/limor_fried.jpg" width="250">

In her keynote, Limor will discuss how Python is snaking its way into hardware, Linux single board computers and more.

Tickets are now available at linux.conf.au so jump on and grab yours to hear what Limor has to say.

## About Limor "Ladyada" Fried

MIT engineer Limor Fried founded Adafruit in 2005.
Her goal was to create the best place online for learning electronics and to make the best designed products for makers of all ages and skill levels.

In the last 10 years, Adafruit has grown to over 100+ employees in the heart of New York City.
Adafruit has expanded offerings to include tools, equipment, and electronics that Limor personally selects, tests, and approves before going into the Adafruit store.

Limor was the first female engineer on the cover of WIRED magazine, awarded Entrepreneur Magazine's Entrepreneur of the Year, and was on the cover of Make: Vol. 57.
She was a founding member of the New York City Industrial Business Advisory Council.
Limor was named a White House Champion of Change in 2016.
In 2018 she became one of "America's Top 50 Women in Tech" by Forbes Magazine.

Adafruit is ranked #11 in the top 20 USA manufacturing companies and #1 in New York City by Inc. 5000 "fastest growing private companies."

During the COVID-19 outbreak Adafruit Industries has been operating as an essential service and manufacturing business making PPE and medical device components.

In 2020 Limor was appointed to the Small Business Sector Advisory Council by the City of New York to help restart the NYC economy post COVID-19 pandemic.

We look forward to welcoming Limor to linux.conf.au 2021.

## But wait... there's more!

We are pleased to announce that our [schedule](/schedule/) has been released!
Wow what a wonderful list of speakers joining us for LCA2021.
Also make sure you lock in your ticket ASAP to be able to get a [Swag Badge](/attend/swag-badge/).
There are limited numbers left :)

## About linux.conf.au

Australasia's grassroots Free and Open Source technologies conference, linux.conf.au, will be held online, worldwide, from Saturday 23 to Monday 25 January 2021.
Running since 1999, linux.conf.au is the largest linux and open source conference in the Asia-Pacific region.
The conference provides deeply technical presentations from industry leaders and experts on a wide array of subjects relating to open source projects, data and open government and community engagement.

## How to get tickets?

Jump onto linux.conf.au to grab your ticket via your [dashboard](/dashboard/).

If you need help convincing your employer to let you attend linux.conf.au 2021, we have a page to assist.
Check out our [why should your employees attend](/attend/why-should-employees-attend/) page, and share it with your manager.
