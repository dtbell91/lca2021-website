---
layout: post
title: "The countdown: One week to LCA2021"
card: countdown.98a1e7ea.png
---

<p class="lead">linux.conf.au 2021 will be beaming into homes and workplaces across the world in just 7 days, from Saturday 23 to Monday 25 January.</p>

Have you been wondering what's next for the world?
Are you keen to explore how the open-source community can help society navigate through these unique times?
Then you need to grab your ticket.

The online format means tickets are at a lower price than previous years.
Jump onto our [dashboard](/dashboard/) to secure yours today.

## Stella lineup of keynotes

Ahem, have you heard who's keynoting at linux.conf.au?
It's a pretty exciting lineup.

### Cory Doctorow
[Cory Doctorow](https://craphound.com) is a science fiction novelist, journalist, and technology activist.
He is a special consultant to the Electronic Frontier Foundation ([eff.org](https://eff.org)), a non-profit civil liberties group that defends freedom in technology law, policy, standards, and treaties.

Let's hear from Cory himself what he has in store for us at linux.conf.au.

<iframe width="560" height="315" src="https://www.youtube.com/embed/5xN-OV1t5PY" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### Omoju Miller

Omoju Miller is the Technical Advisor to the CEO at GitHub.
Her expertise is in the area of machine learning and computational intelligence.

Omoju will discuss participation in open-source as a form of cognitive apprenticeship.
She will map out how the transition step from novice to expert aligns with the progressions we see in the roles taken on in open source communities.

Omoju will share ideas on how to transition from beginner to master faster by accessing what we know from education research.

### Limor Fried

Limor "Ladyada" Fried is an electrical engineer and founder of Adafruit.

In her keynote, Limor will discuss how Python is snaking its way into hardware, Linux single board computers, and more.

## Newcomers Session

Our Newcomers Session is a popular tradition of linux.conf.au.
This year it will take place online on Friday 22 January at 7pm.

Whether this is your first Linux conference or you come every year, this is the place to find out more about the conference and everything it has to offer.
It is also a great place to connect and chat with our community.

BYO beverage of choice!

## Do you need financial assistance to attend linux.conf.au 2021?

Thanks to the support of our Outreach and Inclusion sponsor, Arm, we have funds set aside to provide financial assistance to linux.conf.au attendees who might need it.
This program is part of our outreach and inclusion efforts for linux.conf.au.

Anyone can apply for financial assistance to attend.
You can apply for full or partial financial assistance to cover the costs of what you might need to join the conference.

To find out more about our financial assistance and how to apply, read our [assistance](/attend/assistance/) page.

We look forward to seeing you next week!

